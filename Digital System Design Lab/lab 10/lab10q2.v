module lab10q2(T,clk,Reset,Q);
input T,clk,Reset;
output Q;
reg Q;
always@(negedge Reset or posedge clk)
begin
if(!Reset)
Q<=0;
else
begin
if(!T)
Q<=Q;
else
Q<=~Q;
end
end
endmodule
