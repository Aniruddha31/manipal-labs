module lab10q1(D,clk,Reset,Q);
input D,clk,Reset;
output Q;
reg Q;
always@(posedge Reset or posedge clk)
if(Reset)
Q<=0;
else
Q<=D;
endmodule
