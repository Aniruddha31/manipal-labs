module lab10q4(clk,I,A,Reset);
input clk;
input [0:4]I;
output [0:4]A;
lab10q1 f0(I[0],clk,Reset,A[0]);
lab10q1 f1(I[1],clk,Reset,A[1]);
lab10q1 f2(I[2],clk,Reset,A[2]);
lab10q1 f3(I[3],clk,Reset,A[3]);
lab10q1 f4(I[4],clk,Reset,A[4]);
endmodule
