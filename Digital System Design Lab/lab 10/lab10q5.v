module lab10q5(clk,I,A,Reset);
input clk,Reset;
input I;
output [5:0]A;
lab10q1 f0(I,clk,Reset,A[0]);
lab10q1 f1(A[0],clk,Reset,A[1]);
lab10q1 f2(A[1],clk,Reset,A[2]);
lab10q1 f3(A[2],clk,Reset,A[3]);
lab10q1 f4(A[3],clk,Reset,A[4]);
lab10q1 f5(A[4],clk,Reset,A[5]);
endmodule
