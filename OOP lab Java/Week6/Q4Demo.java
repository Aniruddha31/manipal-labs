import com.course.structure.Building;
import com.course.structure.House;
import com.course.structure.School;

class Q4Demo
{
	public static void main(String[] args) {
		Building ob1 = new Building();
		House ob2 = new House();
		School ob3 = new School();
		ob1.set();
		ob2.set();
		ob3.set();
		ob1.get();
		ob2.get();
		ob3.get();
	}
}

