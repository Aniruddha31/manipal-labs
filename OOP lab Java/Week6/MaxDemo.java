import myPackages.p1.Maximum;
import java. util.*;
import java.lang.*;


class MaxDemo
{
	public static void main(String[] args) {
		//Maximum obj = new Maximum();
		Scanner sc = new Scanner(System.in);

		int a = 10;
		int b = 15;
		int c = 5;
		float x = 23.5f;
		float y = 32.9f;
		float z = 23.9f;
		int arr[] = new int[5];
		int array[][] = new int[2][2];
		System.out.print("Enter elements of arrray: ");
		for(int i = 0; i<arr.length; i++)
		{
			arr[i] = sc.nextInt();
		}

		System.out.print("Enter elements of matrix: ");
		for(int i = 0; i<array.length; i++)
		{
			for(int j = 0; j<array[i].length; j++)
			{
				array[i][j] = sc.nextInt();
			}
		}

		int maxint = Maximum.max(a,b,c);
		System.out.println("Max Int is: "+ maxint);
		float maxfloat = Maximum.max(x,y,z);
		System.out.println("Max float is: "+ maxfloat);
		int max = Maximum.max(arr);
		System.out.println("Array max: "+max);
		max = Maximum.max(array);
		System.out.println("Matrix max is: "+max);
	}
}