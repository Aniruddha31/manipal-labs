import java.util.*;
import java.lang.*;


class Date
{
	int year;
	int day;
	int month;

	Date()
	{
		day=1;
		month=1;
		year=2000;
	}

	void modify()
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter day, month and year: ");
		day=sc.nextInt();
		month=sc.nextInt(); 
		year=sc.nextInt();
	}

	void printdate()
	{
		System.out.println("The dob is: "+day+"/"+month+"/"+year);
	}

}
class Person
{
	private String name;
	private Date dob;

	Person()
	{
		dob = new Date();
	}

	void getdetails()
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter name: ");
		this.name = sc.nextLine();
		dob.modify();
	}

	void printdetails()
	{
		System.out.println("Name is: "+this.name);
		dob.printdate();
	}
}
class CollegeGraduate extends Person
{
	private double gpa;
	private Date gradyear;

	CollegeGraduate()
	{
		gpa=0.0;
		gradyear = new Date();
	}

	void getclgdeatils()
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter gpa: ");
		this.gpa = sc.nextDouble();
		gradyear.modify();
	}

	void printclgdetails()
	{
		System.out.println("GPA of student is: "+gpa);
		gradyear.printdate();
	}

}

class InheritDemo
{
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		CollegeGraduate obj1 = new CollegeGraduate();
		obj1.getdetails();
		obj1.getclgdeatils();
		obj1.printdetails();
		obj1.printclgdetails();
	}
}