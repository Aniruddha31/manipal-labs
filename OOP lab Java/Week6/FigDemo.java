import java.util.*;
import java.lang.*;

abstract class Figure
{
	int a;
	int b;

	abstract double area();
}

class Triangle extends Figure
{
	Triangle(int a, int b)
	{
		this.a=a;
		this.b=b;
	}

	double area()
	{
		double ar = 0.5*this.a*this.b;
		return ar; 
	}
}

class Rectangle extends Figure
{
	Rectangle(int a,int b)
	{
		this.a=a;
		this.b=b;
	}

	double area()
	{
		double ar = a*b;
		return ar; 
	}
}

class Square extends Figure
{
	Square(int a, int b)
	{
		this.a=a;
		this.b=b;
	}

	double area()
	{
		double ar = a*b;
		return ar; 
	}
}


class FigDemo
{
	public static void main(String[] args) {
		Figure obj1 = new Triangle(4, 10);
		double area = obj1.area();
		System.out.println("Area of Triangle is: "+area);
		obj1 = new Rectangle(5, 10);
		area = obj1.area();
		System.out.println("area of Rectangle is: "+area);
		obj1 = new Square(5, 5);
		area = obj1.area();
		System.out.println("Area of Square is: "+area);
	}
}