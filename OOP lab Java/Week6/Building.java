package com.course.structure;
import java.lang.*;
import java.util.*;

public class Building
{
	int sqft;
	int stories;

	public void set()
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter square feet: ");
		this.sqft = sc.nextInt();
		System.out.println("Enter stories: ");
		this.stories = sc.nextInt();
	}

	public void get()
	{
		System.out.println("Square feet: "+this.sqft);
		System.out.println("Stories: "+this.stories);
	}
}