package myPackages.p1;

import java.util.*;
import java.lang.*;


public class Maximum
{
	public static int max(int a, int b, int c)
	{
		return (a>b)?(a>c?a:c):(b>c?b:c);
	}

	public static float max(float a, float b, float c)
	{
		return (a>b)?(a>c?a:c):(b>c?b:c);
	}

	public static int max(int a[])
	{
		int maxi = a[0];
		for(int i = 1; i<a.length; i++)
		{
			if(maxi<a[i])
			{
				maxi=a[i];
			}
		}
		return maxi;
	}

	public static int max(int a[][])
	{
		int maxi = a[0][0];
		for(int i = 0; i < a.length; i++)
		{
			for(int j = 0; j<a[i].length; j++)
			{
				if(a[i][j]>maxi)
					maxi = a[i][j];
			}
		}
		return maxi;
	}

}
