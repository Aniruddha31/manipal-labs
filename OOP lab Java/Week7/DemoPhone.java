import java.util.*;

class Phone 
{
	String brand;
	int memCapacity;

	Phone(String b, int cap)
	{
		this.brand = b;
		this.memCapacity = cap;
	}

	interface Callable
	{
		void makeAudioCall(String cellNum);
		void makeVideoCall(String cellNum);
	}
}

class BasicPhone extends Phone implements Phone.Callable
{
	BasicPhone(String b, int cap)
	{
		super(b,cap);
	}

	public void makeAudioCall(String cellNum)
	{
		System.out.println("Placing call to: "+cellNum);
	}

	public void makeVideoCall(String cellNum)
	{
		System.out.println("Can't place Video call.");
	}
}

class SmartPhone extends Phone implements Phone.Callable
{
	SmartPhone(String b, int cap)
	{
		super(b, cap);
	}

	public void makeAudioCall(String cellNum)
	{
		System.out.println("Smart Phone placing audio call to: "+cellNum);
	}

	public void makeVideoCall(String cellNum)
	{
		System.out.println("Smart Phone placing video call to: "+cellNum);
	}
}

class DemoPhone
{
	public static void main(String[] args) {
		BasicPhone ob1;
		SmartPhone ob2;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter brand: ");
		String brand = sc.nextLine();
		System.out.println("Enter memory capacity: ");
		int mem = sc.nextInt();
		ob1 = new BasicPhone(brand, mem);
		ob2 = new SmartPhone(brand, mem);
		ob1.makeAudioCall("+91-6362938381");
		ob1.makeVideoCall("+91-6362938381");
		ob2.makeAudioCall("+91-6362938381");
		ob2.makeVideoCall("+91-6362938381");

	}
}