import java.util.*;

interface Series
{
	int getNext();
	void reset();
	void setStart(int x);
}

class ByTwos implements Series
{
	int start;
	int term;

	ByTwos()
	{
		start = 0;
		term = 0;
	}

	public void setStart(int x)
	{
		start = x;
		term = x;
	}

	public int getNext()
	{
		int temp = term;
		term = term + 2;
		return temp;
	}

	public void reset()
	{
		start = 0;
		term = start;
	}

}

class SeriesDemo
{
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		ByTwos obj = new ByTwos();
		System.out.println("Enter number of terms: ");
		int n = sc.nextInt();
		int num;
		int i = 0;
		int tru = 1;
		
		while(tru == 1)
		{
			System.out.println("1.Get next term\n2.Reset\n3.Set Start to new value\n4.Exit\nEnter choice: ");
			int choice = sc.nextInt();
			switch(choice)
			{
				case 1:
				num=obj.getNext();
				System.out.println("The next term is: "+num);
				break;

				case 2:
				obj.reset();
				break;

				case 3:
				System.out.println("Enter new start value: ");
				int s = sc.nextInt();
				obj.setStart(s);
				break;

				case 4:
				tru =0;
				break;
			}			
		}
	}
}