import java.util.*;

class student
{
	private static int count;
	private static int yearcount;
	private int regno = 1925;
	private String name = new String();
	private int year;
	private short semester;
	private float gpa;
	private float cgpa;
	GregorianCalendar g = new GregorianCalendar();
	
	static
	{
		count = 5;
	}
	
	student(String name, float gpa, float cgpa, int date, int month, int year, short semester) throws SeatsFilledException
	{
		this.name = name;
		this.gpa = gpa;
		this.cgpa = cgpa;
		g.set(GregorianCalendar.YEAR, year);
		g.set(GregorianCalendar.MONTH, month);
		g.set(GregorianCalendar.DATE, date);
		this.semester = semester;
		regno += count;
		if(regno>1925)
		{
			throw (new SeatsFilledException());
		}
		count = count + 5;
	}

	void display()
	{
		System.out.println("NAME: "+name);
		System.out.println("GPA: "+gpa);
		System.out.println("CGPA: "+cgpa);
		System.out.println("DATEOFJOIN: "+g.get(GregorianCalendar.DATE));
		System.out.println("MONTHOFJOIN: "+g.get(GregorianCalendar.MONTH));
		System.out.println("YEAROFJOIN: "+g.get(GregorianCalendar.YEAR));
		System.out.println("REGNO: "+regno);
	}

	static void sortsem(student s[])
	{
		student temp;
		for(int i=0; i<s.length-1; i++)
		{
			for(int j=0; j<s.length-1-i; j++)
			{
				if(s[j].semester>s[j+1].semester)
				{
					temp = s[j];
					s[j] = s[j+1];
					s[j+1] = temp;
				}
			}
		}
		for(int i=0; i<s.length; i++)
			System.out.println("name: "+s[i].name+" "+"sem: "+s[i].semester);
	}

	static void sortcgpa(student s[])
	{
		student temp;
		for(int i=0; i<s.length-1; i++)
		{
			for(int j=0; j<s.length-1-i; j++)
			{
				if(s[j].cgpa>s[j+1].cgpa)
				{
					temp = s[j];
					s[j] = s[j+1];
					s[j+1] = temp;
				}
			}
		}
		for(int i=0; i<s.length; i++)
			System.out.println("name: "+s[i].name+" "+"cgpa: "+s[i].cgpa);
	}

	static void sortname(student s[])
	{
		student temp;
		for(int i=0; i<s.length-1; i++)
		{
			for(int j=0; j<s.length-1-i; j++)
			{
				if((s[j].name).compareTo((s[j+1]).name)>0)
				{
					temp = s[j];
					s[j] = s[j+1];
					s[j+1] = temp;	
				}
			}
		}	
		for(int i=0; i<s.length; i++)
			System.out.println("name: "+s[i].name);
	}

	static void partchar(student s[], char a)
	{
		for(int i=0; i<s.length; i++)
		{
			if(s[i].name.charAt(0)==a)
			System.out.println(s[i].name);
		}
	}

	static void partsubstring(student s[], String a)
	{
		for(int i=0; i<s.length; i++)
		{
			if(s[i].name.contains(a))
			System.out.println(s[i].name);
		}
	}

	static void initials(student s[])
	{
		int j;
		String str[];
		for(int i=0; i<s.length; i++)
		{
			StringBuffer sb = new StringBuffer();
			str = s[i].name.split(" ");
			for(j=0; j<str.length-1; j++)
			{
				sb.append(str[j].charAt(0));
				sb.append(". ");
			}
			sb.append(str[j]);
			s[i].name = new String(sb);
		}	
		for(int i=0; i<s.length; i++)
			System.out.println("name: "+s[i].name);
	}
}

class SeatsFilledException extends Exception
{
	public String toString()
	{
		return "Seats have been filled. You are no longer eligible for enrollment.";
	}
}

class StudentException
{
	public static void main(String[] args)
	{
		int n;
		String name = new String();
		float gpa, cgpa;
		int year;
		int date;
		int month;
		short semester;
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter no. of students: ");
		n = sc.nextInt();
		student s[] = new student[n];
		sc.nextLine();
		for(int i=0; i<n; i++)
		{
			System.out.print("Enter name: ");
			name = sc.nextLine();
			System.out.print("Enter gpa: ");
			gpa = sc.nextFloat();
			System.out.print("Enter cgpa: ");
			cgpa = sc.nextFloat();
			System.out.print("Enter dateofjoin: ");
			date = sc.nextInt();
			System.out.print("Enter monthofjoin: ");
			month = sc.nextInt();
			System.out.print("Enter yearofjoin: ");
			year = sc.nextInt();
			System.out.print("Enter semester: ");
			semester = sc.nextShort();
			try
			{
				s[i] = new student(name, gpa, cgpa, date, month, year, (short)semester);	
			}

			catch(SeatsFilledException e)
			{
				System.out.println(e);
			}
			sc.nextLine();
		}
		System.out.println("************************");
		System.out.println("Sortedbyname\n");
		student.sortname(s);
		System.out.println("************************");
		System.out.println("Sortedbysem\n");
		student.sortsem(s);
		System.out.println("************************");
		System.out.println("Sortedbycgpa\n");
		student.sortcgpa(s);
		System.out.println("************************");
		System.out.print("Enter character: ");
		char c = sc.nextLine().charAt(0);
		student.partchar(s, c);
		System.out.println("************************");
		System.out.print("Enter substring: ");
		String sub = sc.nextLine();
		student.partsubstring(s, sub);
		System.out.println("************************");
		System.out.println("PrintingInitials\n");
		student.initials(s);
		System.out.println("************************");
	}
}
