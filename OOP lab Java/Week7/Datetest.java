import java.util.*;

class CurrentDate
{
	int day, month, year;

	CurrentDate(int d, int m, int y)
	{
		this.day = d;
		this.month = m;
		this.year = y;
		System.out.println("Object was made successfully!");
	}

	static CurrentDate createDate() throws InvalidDayException, InvalidMonthException
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter day: ");
		int d = sc.nextInt();
		System.out.println("Enter month: ");
		int m = sc.nextInt();
		System.out.println("Enter year: ");
		int y = sc.nextInt();
		
		
		if (y%4==0)
		{
			if(y%100==0)
			{
				if(y%400==0)
				{
					if(m==2)
					{
						if(d>29 && d < 1)
							throw(new InvalidDayException());
					}
				}
			}
		}

		if(m > 12 || m < 1)
		{
			throw(new InvalidMonthException());
		}

		else if((m<8 && m%2==0)||(m>8&&m%2!=0))
		{
			if(d>30 || d < 1)
			{
				throw(new InvalidDayException());
			}
		}

		else if((m<8 && m%2!=0) ||(m>7&&m%2==0))
		{
			if(d>31 || d < 1)
			{
				throw(new InvalidDayException());
			}
		}

		return new CurrentDate(d,m,y);
	}
}

class InvalidDayException extends Exception
{
	public String toString()
	{
		return "Wrong day entered. Please enter day less than 31.";
	}
}

class InvalidMonthException extends Exception
{
	public String toString()
	{
		return "Wrong month entered. Please enter month less than 12.";
	}
}

class Datetest
{
	public static void main(String[] args) {
		try
		{
			CurrentDate obj = CurrentDate.createDate();
		}

		catch(InvalidDayException e)
		{
			System.out.println(e);
		}

		catch(InvalidMonthException e1)
		{
			System.out.println(e1);
		}
	}
}