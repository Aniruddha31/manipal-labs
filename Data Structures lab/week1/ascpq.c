#include<stdio.h>
#include<stdlib.h>
#define MAX 10

typedef struct
{
	int item[MAX];
	int front,rear;
}pqueue;

void pqinsert(pqueue* pq, int x)
{
	if(pq->rear >= MAX)
	{
		printf("Queue overflow!\n");
		return;
	}

	int temp = pq->front;
	while(temp!=pq->rear)
	{
		if(x<pq->item[temp])
		{
			for(int i = pq->rear; i>=temp; i--)
			{
				pq->item[i+1] = pq->item[i];
			}

			pq->item[temp] = x;
			temp++;
			pq->rear++;
			return;
		}
		temp++;
	}

	pq->item[pq->rear] = x; 
	pq->rear++;
}

int pqdelete(pqueue* pq)
{
	if(pq->front==pq->rear)
	{
		printf("Queue Empty\n");
	}
	return pq->item[(pq->front)++];
}

void display(pqueue* pq)
{
	if(pq->front==pq->rear)
	{
		printf("Queue empty!\n");
		return;
	}	
	int i = 0;
	while(i < pq->rear)
	{
		printf("%d \n", pq->item[i]);
		i++;
	}
}

int main(int argc, char const *argv[])
{
	pqueue pq;
	pq.front = pq.rear = -1;
	int choice;
	
	do {
		printf("1. Insert\n2. Delete\n3. Display\nEnter choice :\n");
		scanf("%d",&choice);
		int x;
		if (choice == 1) 
		{
			printf("\nEnter item to be inserted: ");
			scanf("%d",&x);
			pqinsert(&pq,x);
		}
		else if (choice == 2) 
		{
			int x = pqdelete(&pq);
			printf("%d was deleted!\n",x);
		}
		else if (choice == 3) 
		{
			display(&pq);		
		}
		
	} while (choice >= 1 && choice < 4);




	/*pqinsert(&pq, 10);
	pqinsert(&pq, 20);
	pqinsert(&pq, 4);
	pqinsert(&pq, 3);
	
	pqinsert(&pq, 5);
	display(&pq);*/
	
	return 0;
}