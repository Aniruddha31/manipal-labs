#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define MAX 40
#define UFLOW '\0'

typedef enum{False,True} Bool;
typedef struct 
{
	char** arr;
	int front,rear,capacity;
}DQUEUE;
typedef DQUEUE* dqptr;

void init(dqptr dq)
{
	dq->arr=(char**)calloc(MAX,sizeof(char*));
	dq->front=dq->rear=-1;
	dq->capacity=0;
    for(int i=0;i<MAX;i++)
		dq->arr[i]=(char*)calloc(MAX,sizeof(char));
}

Bool isfull(dqptr dq)
{
	if(dq->capacity==MAX)
	{
		return True;
	}
	else
		return False;
}

Bool isempty(dqptr dq)
{
	if(dq->capacity==0)
		return True;
	else
		return False;
}

void insertRight (dqptr dq, char* item)
{
	if (dq->rear == MAX - 1) 
	{
		printf("QUEUE RIGHT OVERFLOW\n\n");
		return;
	}
	
	if (isempty(dq))
		dq->front = dq->rear = 0;
	else
		dq->rear += 1;
	
	dq->capacity += 1;
	
	strcpy(dq->arr[dq->rear],item);
}

void insertLeft (dqptr dq, char * item) 
{
	if (dq->front == - 1) 
	{
		printf("QUEUE LEFT OVERFLOW\n\n");
		return;
	}
	
	if (isempty(dq))
		dq->front = dq->rear = 0;
	else
		dq->front += 1;
	
	dq->capacity += 1;
	
	strcpy(dq->arr[dq->front],item);
}

char* deleteLeft(dqptr dq)
{

	if(isempty(dq))
	{
		printf("QUEUE EMPTY\n");
		return UFLOW;
	}
   char* item = dq->arr[dq->front];
	dq->front+=1;
	dq->capacity-=1;
	if(isempty(dq))
		dq->front=dq->rear=-1;
	return item;
}

char* deleteRight(dqptr dq)
{
	if(isempty(dq))
	{
		printf("QUEUE EMPTY\n");
		return UFLOW;
	}
    char * item = dq->arr[dq->rear];
	dq->rear-=1;
	dq->capacity-=1;
	if(isempty(dq))
		dq->front=dq->rear=-1;
	return item;
}
void display (dqptr dq)
{
	int i;
	if (!isempty(dq))
	{
		for (i = 0; i < dq->capacity; ++i)
			printf("%s\t\t",dq->arr[i]);
		printf("\n\n");
	}
}

int main() 
{
	
	dqptr dq = (dqptr)calloc(MAX, sizeof(DQUEUE));
	init(dq);	
	int choice;
	
	do {
		printf("1. Insert Right\n2. Insert Left\n3. Delete Left\nEnter choice :\n");
		scanf("%d",&choice);
		char * item = (char *)malloc(MAX * sizeof(char));
		if (choice == 1) 
		{
			printf("\nEnter item to be inserted: ");
			scanf("%s",item);
			insertRight(dq, item);
		}
		else if (choice == 2) 
		{
			printf("\nEnter item to be inserted: ");
			scanf("%s",item);
			insertLeft(dq, item);
		}
		else if (choice == 3) 
		{
			item=deleteLeft(dq);
			if (item != UFLOW)
				printf("Deleted item: %s\n", item);
		}
		
	} while (choice >= 1 && choice <= 4);
	
	return 0;
}
