#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define MAX 10
#define UFLOW '\0'

typedef enum{False,True} Bool;
typedef struct 
{
	char arr[MAX];
	int front,rear,capacity;
}DQUEUE;
typedef DQUEUE* dqptr;

void init(dqptr dq)
{
	//dq->arr=(char**)calloc(MAX,sizeof(char*));
	dq->front=dq->rear=-1;
	dq->capacity=0;
    //for(int i=0;i<MAX;i++)
	//	dq->arr[i]=(char*)calloc(MAX,sizeof(char));
}

Bool isfull(dqptr dq)
{
	if(dq->capacity==MAX)
	{
		return True;
	}
	else
		return False;
}

Bool isempty(dqptr dq)
{
	if(dq->capacity==0)
		return True;
	else
		return False;
}

void insert(dqptr dq, char item)
{
	if (dq->rear == MAX - 1) 
	{
		printf("QUEUE RIGHT OVERFLOW\n\n");
		return;
	}
	
	if (isempty(dq))
		dq->front = dq->rear = 0;
	else
		dq->rear += 1;
	
	dq->capacity += 1;
	
	dq->arr[dq->rear] = item;
}

char dequeue1(dqptr dq)
{
	if(isempty(dq))
	{
		printf("Queue empty!\n");
		return "\0";
	}
	char c;
	c = dq->arr[dq->front];
	if (dq->rear==dq->front)
	{
		init(dq);
	}
	else
	{
		p->front = (p->front+1)%MAX;
		return c;
	}
}

char dequeue2(dqptr dq)
{
	if(isempty(dq))
	{
		printf("Queue empty!\n");
		return '\0';
	}
	char c;
	c = dq->arr[dq->rear];
	if (dq->rear==dq->front)
	{
		init(dq);
	}
	else
	{
		dq->front = (dq->rear-1=MAX)%MAX;
		return c;
	}
}

void disp(dqptr dq)
{
	int i;
	for(i = dq->front+1; i!=dq->rear; i++)
	{
		i=i%MAX;
		printf("%c\n",dq->arr[i]);
	}
	printf("%c\n",dq->arr[i]);
}

int main(int argc, char const *argv[])
{
	DQUEUE dq;
	dq.front = dq.rear = -1;
	char temp[100];
	char f,s;
	printf("Enter string: ");
	scanf("%s", temp);
	for(int i = 0; temp[i]!='\0'; i++)
	{
		insert(&dq,temp[i]);
	}

	int flag = 1;
	int i =0;
	while(i<strlen(temp)/2)
	{
		f = dequeue1(&dq);
		s = dequeue2(&dq);
		if(f!=s)
		{
			flag=0;
			break;
		}
		i++;
	}
	if(flag==0)
		printf("Palindrome!\n");
	else
		printf("Not a Palindrome\n");
	return 0;
}