#include <stdio.h>
#include <stdlib.h>

void push(int stack[], int *n, int *top, int c)
{
	stack[++(*top)]=c;
}
int main()
{
	printf("Enter digit to convert to binary\n");
	int n;
	scanf("%d",&n);
	int stack[100];
	int top;
	top = -1;
	int c;
	
	while(n!=0)
	{		
		if(n%2==0)
			push(stack , &n , &top ,0);
		else
			push(stack , &n , &top ,1);
		n=n/2;
	}
	
	for(int i=0; i< top+1; i++)
	printf("%d ",stack[i]);
}