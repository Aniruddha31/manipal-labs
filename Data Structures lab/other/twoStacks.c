#include <stdio.h>
#define MAX_SIZE 100

char Stack[MAX_SIZE];

int top[]={-1,MAX_SIZE/2};

int boundary[] = {MAX_SIZE/2,MAX_SIZE};

int isFull(int i)
{
	if(top[i]==boundary[i]-1)
		return 1;
	else
		return 0;
}

int isEmpty(int i)
{
	if(top[i]== -1 + (MAX_SIZE/2)*i)
		return 1;
	else
		return 0;
}

void push(int i, char item)
{
	if(isFull(i)==1)
	{
		printf("Stack overflow");
		return  ;
	}

	Stack[++top[i]]=item;
}

char pop(int i)
{
	if(isEmpty(i)==1)
	{
		printf("Stack overflow\n");
		return '\0';
	}

	return Stack[top[i]--];
}

void display(int i)
{
	if(isEmpty(i)==1)
	{
		printf("Stack isEmpty\n");
		return ;
	}
	for(int j=top[i]; j> -1 + (MAX_SIZE/2)*i;j--)
	{
		printf("%c\n",Stack[j] );
	}
}

int main(void)
{
	push(0,'x');
	push(1,'A');
	push(0,'y');
	push(1,'B');
	push(0,'z');
	push(1,'C');
	pop(0);
	pop(1);

	printf("Stack 0\n");
	display(0);
	printf("Stack 1\n");
	display(1);
}