#include <stdio.h>
#include <stdlib.h>
typedef struct
{
	int a[100];
	int top1;
	int top2;
}stack;

void intitalize(stack *s, int n)
{
     s->top1=-1;
     s->top2=n;
}

void print1(stack *s)
{
    for(int i=s->top1; i>= 0; i--)
	    printf("%d ",s->a[i]);
	printf("\n");
}

void print2(stack *s, int n)
{
    for(int i=n-1; i>= s->top2; i--)
	    printf("%d ",s->a[i]);
	printf("\n");
}

void pop1(stack *s)
{
	if(s->top1== -1)
	{
		printf("\nempty");
	}
	else
	{
	printf("%d\n",s->a[s->top1]);
	s->top1 -=1;
    }
}

void pop2(stack *s, int n)
{
	if(s->top2== n-1)
	{
		printf("\nempty");
	}
	else
	{
	printf("%d\n",s->a[s->top2]);
	s->top2 +=1;
    }
}

void push1(stack *s, int n)
{
	if((s->top2 - s->top1) >= 2)
	{
	printf("enter element to be pushed \n");
    int p;
    scanf("%d",&p);
        s->top1+=1;
	s->a[s->top1]=p;
    }
    else
    	printf("\nno space");
}

void push2(stack *s, int n)
{
	if((s->top2 - s->top1) >= 2)
	{
	printf("enter element to be pushed \n");
    int p;
    scanf("%d",&p);
	s->a[s->top2]=p;
	s->top2-=1;
	}
    else
    	printf("\nno space");
}

int main()
{
	printf("Enter length of array\n");
	int n;
	scanf("%d",&n);
	stack s;
	int choice =1;
	intitalize(&s,n);
	while(choice !=0)
	{		
		//printf("Enter\n1 push to stack 1 \n2 pop from stack 1 \n3 print stack 1 \n4 push to stack 2 \n5 pop from stack 2 \n6 print stack 2 \n");
		scanf("%d",&choice);
		if(choice == 1)
			push1(&s,n);
		else if(choice == 2)
			pop1(&s);
		else if(choice == 3)
			print1(&s);
		else if(choice == 4)
			push2(&s,n);
		else if(choice == 5)
			pop2(&s,n);
		else if(choice == 6)
			print2(&s,n);
		else
		exit(0);
	}
}