#include <stdio.h>
#include <string.h>
#define MAX_SIZE 100

char Stack[MAX_SIZE];

int top[]={-1,MAX_SIZE/2};

int boundary[] = {MAX_SIZE/2,MAX_SIZE};

int isFull(int i)
{
	if(top[i]==boundary[i]-1)
		return 1;
	else
		return 0;
}

int isEmpty(int i)
{
	if(top[i]== -1 + (MAX_SIZE/2)*i)
		return 1;
	else
		return 0;
}

void push(int i, char item)
{
	if(isFull(i)==1)
	{
		printf("Stack overflow");
		return  ;
	}

	Stack[++top[i]]=item;
}

char pop(int i)
{
	if(isEmpty(i)==1)
	{
		printf("Stack underflow\n");
		return '@';
	}

	return Stack[top[i]--];
}

void display(int i)
{
	if(isEmpty(i)==1)
	{
		printf("Stack isEmpty\n");
		return ;
	}
	for(int j=top[i]; j> -1 + (MAX_SIZE/2)*i;j--)
	{
		printf("%c",Stack[j] );
	}
}

void convert(char * prefix)
{
	int i,popall=0,operand=0,operator=0;   //popall=1 means pop all, popall=0 mean pop first two
    //char postfix[25] ={'\0'};
    char symbol;

    for(i=strlen(prefix) - 1; i>=0; i--)     
    {
        symbol = prefix[i];

        if((symbol>='a' && symbol <='z') || (symbol >='A' && symbol<='Z'))
        {
        	 ++operand;
        	 //printf("\noperand=%d\n", operand);
             push(0,symbol);

             if(operand==(operator+1))
             	popall=1;
             else
             	popall=0;
        }
        else
        {
        	++operator;

        	if(operand==(operator+1))
             	popall=1;
             else
             	popall=0;

        	if(popall==0)  //popall=0 mean pop first two
        	{
        		//printf("\npopall=%d\n", popall);
                push(1,pop(0));
                push(1,pop(0));
                push(0,symbol);
                push(0,pop(1));
                push(0,pop(1));
        	}
        	else         // popall=1 means pop all
        	{
                while(isEmpty(0)!=1)
                {
                	push(1,pop(0));
                }
                push(0,symbol);
                while(isEmpty(1)!=1)
                {
                	push(0,pop(1));
                }
        	}
        }
        

    }
    display(0);
}


int main(void)
{
	/*push(0,'x');
	push(1,'A');
	push(0,'y');
	push(1,'B');
	push(0,'z');
	push(1,'C');
	pop(0);
	pop(1);

	printf("Stack 0\n");
	display(0);
	printf("Stack 1\n");
	display(1);*/

	char prefix[25];
	printf("Enter expression :");
	scanf("%s", prefix);
	convert(prefix);
	return 0;


}