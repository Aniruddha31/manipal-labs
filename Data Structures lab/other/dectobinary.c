#include <stdio.h>
typedef struct
{
	int a[100];
	int top;
}stack;
void print(stack *s)
{
    for(int i=s->top; i>=0; i--)
	    printf("%d",s->a[i]);
	printf("\n");
}
void push(stack *s, int x)
{
        s->top+=1;
	s->a[s->top]=x;
}
int main()
{
	printf("Enter number\n");
	int n;
	scanf("%d",&n);
	int num = n;
	int size=0;
	stack s;
	s.top=-1;
	while(n>0)
	{
		push(&s,n%2);
		n = n/2;
	}
	print(&s);
	return 0;
}