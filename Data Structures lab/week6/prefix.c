#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>

typedef struct
{
	int arr[100];
	int top;

}stack;

int isOperand(char c)
{
	return isdigit(c);
}

double evaluate(char str[])
{
	stack s;
	s.top=-1;

	for(int j=strlen(str)-1;j>=0;j--)
	{
		printf("%c\n",str[j]);
		if(isOperand(str[j]))
		{
			//printf("zzz\n");
			s.top++;
			s.arr[s.top]=str[j]-'0';
		}

		else
		{
			//printf("yyy\n");
			double op1=s.arr[s.top];
			//printf("zzz%lf\n",op1);
			s.top--;
			double op2=s.arr[s.top];
			//printf("yyy%lf\n",op2);
			s.top--;

			switch(str[j])
			{
				case '+':
					s.top++;
					s.arr[s.top]=op1+op2;
					break;
				case '-':
					s.top++;
					s.arr[s.top]=op1-op2;
					break;
				case '*':
					s.top++;
					s.arr[s.top]=op1*op2;
					break;
				case '/':
					s.top++;
					s.arr[s.top]=op1/op2;
			}
		}
	}

	//s.top--;
	return (double)s.arr[s.top];
}

int main()
{
	char str[100];
	printf("Enter prefix expression:");
	scanf("%s",str);
	printf("%lf\n",evaluate(str));
	return 0;
}