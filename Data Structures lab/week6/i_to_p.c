#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>

typedef struct
{
	char arr[100];
	int top;

}stack;

void reverse(stack *in)
{
	int i,j;
	char temp;

	for(i=0;i<strlen(in->arr)/2;i++)
	{
		temp=in->arr[i];
		in->arr[i]=in->arr[strlen(in->arr)-1-i];
		in->arr[strlen(in->arr)-1-i]=temp;
	}
}

int prcd(symbol)
{
	switch symbol
	{
		case '+':
		case '-':
			return 2;
			break;
		case '*':
		case '/':
			return 4;
			break;
		case '$':
		case '^':
			return 6;
			break;
		case '#':
		case '(':
		case ')':
			return 1;
	}
}

int isOperator(char symbol)
{
	case '+':
	case '-':
	case '*':
	case '/':
	case '$':
	case '^':
	case '#':
	case '(':
	case ')':
			return 1;
			break;
	default:
		return 0;
}

void infix_to_prefix(stack *in,stack *pre)
{
	int i,j=0;
	char symbol,stack[100],top=-1;
	stack[++top]='#';
	//pre->top=0;
	//pre->arr[pre->top]='#';

	reverse(&in);

	for(i=0;i<strlen(in->arr);i++)
	{
		symbol=in->arr[i];

		if(isOperator(symbol)==0)
		{
			pre->arr[j]=symbol;
			j++;
		}
		else
		{
			if(symbol==')')
			{
				top++;
				stack[top]=symbol;
			}
			else if(symbol=='(')
			{
				while(stack[top]!=')')
				{
					pre->arr[j]=stack[top];
					top--;
					j++;
				}
				top--;
			}
			else
			{
				if(prcd(stack[top])<=prcd(symbol))
				{
					top++;
					stack[top]=symbol;
				}
				else
				{
					while(prcd(stack[top])<=prcd(symbol))
					{
						pre->arr[j]=stack[top];
						top--;
						j++;
					}
					top++;
					stack[top]=symbol;
				}
			}
		}
	}

	while(stack[top]!='#')
	{
		pre->arr[j]=stack[top];
		top--;
		j++;
	}
	pre->arr[j]='\0';
}

int main()
{
	stack in,pre;
	printf("Enter infix expression:");
	scanf("%s",in.arr);
	infix_to_prefix(&in,&pre);
	reverse(&pre);
	printf("%s",pre.arr);


	return 0;
}