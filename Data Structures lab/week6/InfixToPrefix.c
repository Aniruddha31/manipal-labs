#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define MAXSIZE 25

typedef struct 
{
	char item[MAXSIZE];
	int top;
}STACK;

int isfull(STACK * ps)
{
	if(ps->top == MAXSIZE -1)
		return 1;
	return 0;
}

int isempty(STACK * ps)
{
	if(ps->top == -1)
		return 1;
	return 0;
}

void push(STACK * ps , char  ele)
{
	if(isfull(ps))
	{
		printf("stack overflow\n" );
		return ;
	}
	//printf("pushed =%c\n", ele);
	ps->item[++(ps->top)]=ele;
}

char pop(STACK * ps)
{
	if(isempty(ps))
	{
		printf("underflow\n");
		return '\0';
	}
	 //printf("poped=%c\n",(ps->item[(ps->top)--]));

	 return (ps->item[(ps->top)--]);
}

int precedence(char ch)
{
	switch(ch)
	{
		case '+' :
		case '-' : return 1;
		case '*' : 
		case '/' : 
		case '%' : return 2;
		case '^' : return 3;
	}
	return -1;
}

void convert (char * infix, STACK *s)
{
	int i, temp =0;
	char next ;
	char symbol ,waste;
	char prefix[25] ={'\0'};
	push(s,')');

	//printf("length=%ld\n",strlen(infix) );

	for(i=strlen(infix) - 1; i>=0; i--)       //scanning from behind
	{
		symbol = infix[i];
		//printf("%c\n", symbol);
		if((symbol>='a' && symbol <='z') || (symbol >='A' && symbol<='Z'))
			prefix[temp++]=symbol;
		else
		{   //printf("%c\n", symbol);
			switch(symbol)
			{
				case ')' : push(s,symbol); break;
				case '(' ://printf("popped=%c",pop(s));
				            while(s->item[s->top]!=')')
				               { prefix[temp++]= pop(s);
				                   s->top = --(s->top); //printf("top=%d",s->top);    (WHY DO WE ADD 41 FIND OUT)
                                  //printf("next=%c",next+41);
				               }
				           break;
				case '+' : 
				case '-' :
				case '*' : 
				case '/' :
				case '%' :
				case '^' : while(!isempty(s) && (precedence(s->item[s->top]))>precedence(symbol))
				           {
				           	  //waste = pop(s);
				           	  //printf("waste=%c\n",waste);
				           	  //printf("top=%d",s->top);
				           	  prefix[temp++]=pop(s);
				           	  //printf("%s",prefix);
				           }
				           push(s,symbol);
				           break;
			}
		}

		//prefix[temp]='\0';
	}

    printf("top out =%d",s->top);
	for(int j=0; j<=s->top; j++)
		printf("%c\t", s->item[j]);
	printf("\n");
	while((next=pop(s))!=')')
	{
		printf("%c\t",next);
		prefix[temp++]=next;
	}

	for(int j=0; j<strlen(prefix)/2; j++)      //reversinng prefix array
	{
		char temp= prefix[j];
		prefix[j]=prefix[strlen(prefix)-j-1];
		prefix[strlen(prefix)-j-1]=temp;
	}

	printf("%s",prefix);
	
}

int main()
{
	STACK s1;
	STACK *s;
	s=&s1;
	s->top=-1;
	char infix[25];
	printf("Enter expression :");
	scanf("%s", infix);
	convert(infix,s);
	return 0;
}