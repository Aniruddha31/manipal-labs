#include<stdio.h>
#include<stdbool.h>
#define MAX_SIZE 100

typedef struct 
{
	int arr[MAX_SIZE];
	int front;
	int rear;
}QUEUE;

void init(QUEUE *q)
{
	q->front=-1;
	q->rear=-1;
}

int isFull(QUEUE *q)
{
	if(q->front==(q->rear+1)%MAX_SIZE)
		return 1;
	else
		return 0;
}

int isEmpty(QUEUE *q)
{
	if(q->front==q->rear==-1)
		return 1;
	else
		return 0;
}

void insertcq(QUEUE *q,int s)
{
	if (isFull(q))
	{
		printf("Queue Full\n");
		return;
	}
	q->rear=(q->rear+1)%MAX_SIZE;
	q->arr[q->rear]=s;
	if(q->front==-1)
		q->front=0;
}

int deletecq(QUEUE *q)
{
	if(isEmpty(q))
	{
		printf("Queue Empty\n");
		
		return '\0';
	}
	char x=q->arr[q->front];
	if(q->front==q->rear)
	{
		q->front=q->rear=-1;
	}
	else
		q->front=(q->front+1)%MAX_SIZE;
	return x;
}

void displaycq(QUEUE *q)
{
	printf("Queue: ");
	int i;
	for(i=q->front;i!=q->rear;i=(i+1)%MAX_SIZE)
	{
		printf("%c\t",q->arr[i]);
	}
	printf("%c\n",q->arr[i]);
}

bool searchcq()
{	QUEUE que[MAX_SIZE];
	QUEUE *q=que;
	int n;
	printf("Enter no. of elements in queue: ");
	scanf("%d",&n);
	printf("Enter queue, front and rear: ");
	for(int j=0;j<n;j++)
		scanf("%d",&(q->arr[j]));
	scanf("%d",&(q->front));
	scanf("%d",&(q->rear));
	printf("Enter element to search: ");
	int x;
	scanf("%d",&x);

	int i,r=false; int ch;
	int j=q->rear;
	for(i=q->front;i!=j;i=(i+1)%MAX_SIZE)
	{
		ch=deletecq(q);
		if(ch==x)
			r=true;
		insertcq(q,ch);
	}
	ch=deletecq(q);
		if(ch==x)
			r=true;
		insertcq(q,ch);
		return r;
}

int main(void)
{
	
	bool r;
	r=searchcq();
	if(r)
		printf("Element found");
	else
		printf("Element not found");

	return 0;
}