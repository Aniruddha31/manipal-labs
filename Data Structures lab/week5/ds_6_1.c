#include<stdio.h>
#define MAX_SIZE 4
typedef struct 
{
	char str[MAX_SIZE];
}STRING;

typedef struct 
{
	STRING arr[MAX_SIZE];
	int front;
	int rear;
}QUEUE;

void init(QUEUE *q)
{
	q->front=-1;
	q->rear=-1;
}

int isFull(QUEUE *q)
{
	if(q->front==(q->rear+1)%MAX_SIZE)
		return 1;
	else
		return 0;
}

int isEmpty(QUEUE *q)
{
	if(q->front==q->rear==-1)
		return 1;
	else
		return 0;
}

void insertcq(QUEUE *q,STRING s)
{
	if (isFull(q))
	{
		printf("Queue Full\n");
		return;
	}
	q->rear=(q->rear+1)%MAX_SIZE;
	q->arr[q->rear]=s;
	if(q->front==-1)
		q->front=0;
}

STRING deletecq(QUEUE *q)
{
	if(isEmpty(q))
	{
		printf("Queue Empty\n");
		STRING x={"\0"};
		return x;
	}
	STRING x=q->arr[q->front];
	if(q->front==q->rear)
	{
		q->front=q->rear=-1;
	}
	else
		q->front=(q->front+1)%MAX_SIZE;
	return x;
}

void displaycq(QUEUE *q)
{
	printf("Queue: ");
	int i;
	for(i=q->front;i!=q->rear;i=(i+1)%MAX_SIZE)
	{
		printf("%s\t",q->arr[i].str);
	}
	printf("%s\n",q->arr[i].str);
}

int main(void)
{
	QUEUE q;
	init(&q);
	STRING x={"A"};
	insertcq(&q,x);x.str[0]='B';
	insertcq(&q,x);x.str[0]='C';
	insertcq(&q,x);x.str[0]='D';
	insertcq(&q,x);x.str[0]='E';
	insertcq(&q,x);
	printf("Element deleted:%s\n",(deletecq(&q)).str);
	insertcq(&q,x);x.str[0]='F';
	printf("Element deleted:%s\n",(deletecq(&q)).str);
	insertcq(&q,x);
	displaycq(&q);
	return 0;
}