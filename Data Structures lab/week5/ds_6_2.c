#include<stdio.h>
#define MAX_SIZE 4


typedef struct 
{
	int arr[MAX_SIZE];
	int front[2];
	int rear[2];
	int boundary[2];
}QUEUE;

void init(QUEUE *q)
{
	q->front[0]=-1;
	q->rear[0]=-1;
	q->boundary[0]=MAX_SIZE/2;
	q->front[1]=MAX_SIZE/2-1;
	q->rear[1]=MAX_SIZE/2-1;
	q->boundary[1]=MAX_SIZE;
}

int isFull(QUEUE *q,int i)
{int x=(q->rear[i]+1)%q->boundary[i];
	if(i==1&&x==0)
		x+=MAX_SIZE/2;
	if(q->front[i]==x)
		return 1;
	else
		return 0;
}

int isEmpty(QUEUE *q,int i)
{
	if(q->front[i]==q->rear[i]==-1+(MAX_SIZE/2)*i)
		return 1;
	else
		return 0;
}

void insertcq(QUEUE *q,int i,int s)
{
	if (isFull(q,i))
	{
		printf("Queue Full\n");
		return;
	}
	q->rear[i]=(q->rear[i]+1)%q->boundary[i];
	if(i==1&&q->rear[i]==0)
		q->rear[i]+=MAX_SIZE/2;
	q->arr[q->rear[i]]=s;
	if(q->front[i]==-1+(MAX_SIZE/2)*i)
		q->front[i]++;
}

int deletecq(QUEUE *q,int i)
{
	if(isEmpty(q,i))
	{
		printf("Queue Empty\n");
		
		return -1;
	}
	int x=q->arr[q->front[i]];
	if(q->front[i]==q->rear[i])
	{
		q->front[i]=q->rear[i]=-1+(MAX_SIZE/2)*i;
	}
	else
	{	q->front[i]=(q->front[i]+1)%q->boundary[i];
	if(i==1&&q->front[i]==0)
		q->front[i]+=MAX_SIZE/2;
}
	return x;
}

void displaycq(QUEUE *q,int i)
{
	printf("Queue %d: ",i);
	int j;
	for(j=q->front[i];j!=q->rear[i];j=(j+1)%q->boundary[i])
	{
		if(i==1&&j==0)
		{
			j+=MAX_SIZE/2;
			break;
		}
		printf("%d\t",q->arr[j]);
	}
	printf("%d\n",q->arr[j]);
}

int main(void)
{
	QUEUE q;
	init(&q);
	
	insertcq(&q,0,0);
	
	insertcq(&q,0,1);
	
	insertcq(&q,0,2);
	insertcq(&q,1,2);
	
	insertcq(&q,1,3);

	insertcq(&q,1,4);
	printf("Element deleted:%d\n",deletecq(&q,0));
	insertcq(&q,0,10);

	printf("Element deleted:%d\n",deletecq(&q,1));
	insertcq(&q,1,12);
	displaycq(&q,0);
	displaycq(&q,1);
	return 0;
}